#ifndef CONFIGMANAGER_H
#define CONFIGMANAGER_H
#include <QFile>
#include <QObject>
#include <QDebug>
#include <QRegExp>
#include "definitions.h"


#define FILE_NAME "/home/alireza/Desktop/MRL/Player/Config/Config_OP.lua"

class ConfigManager
{
public:
    ConfigManager();

    void load_config();
    void save_config();
    void reset_tokenList();
    void insert_token(Token);

    QList<Token> get_tokenList(){return tokenList;}
    QList<QFile*> get_qfileList(){return qlistFile;}

private:
    QString linel;
    QStringList list;
    QStringList finalList;
    QString oldValue;
    QStringList fileList;
    QStringList finalFileList;
    QList <QFile*> qlistFile;

    QString fileAddress;

    QList<Token> tokenList;
    int indexer = 0;
    QString fileNameList;
};

#endif // CONFIGMANAGER_H
