#ifndef DEFINITIONS_H
#define DEFINITIONS_H
#include <QString>

enum Tags{
    Ip=0,Number=1,Boolean=2,String=3,Array=4
};

struct Token{
    QString key;
    QString value;
    Tags tag;
    int lineNumber;
    int fileAddress;
    QString fileName;
};

#endif // DEFINITIONS_H
