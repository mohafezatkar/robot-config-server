#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QDebug>

class Server : public QObject
{
    Q_OBJECT
public:
    explicit Server(QObject *parent = 0);

signals:

public slots:
    void newConnection();
    void clientRead();
private:
    QTcpServer* serverSocket;
    QTcpSocket* client;
};

#endif // SERVER_H
