#include "server.h"
#include <QJsonObject>
#include <QJsonDocument>

Server::Server(QObject *parent) : QObject(parent)
{
    serverSocket = new QTcpServer();
    connect(serverSocket, SIGNAL(newConnection()),this, SLOT(newConnection()));

    serverSocket->listen(QHostAddress::Any,5056);
    qDebug()<<"server running on "<<5056;

}

void Server::newConnection(){
    client = serverSocket->nextPendingConnection();
    connect(client, SIGNAL(readyRead()),this, SLOT(clientRead()));
    qDebug()<<"Client connected";

    QJsonObject json;

    QJsonObject json1;
    json1.insert("X","Parizad");
    json1.insert("X",2);
    json1.insert("Y",-3.25);
    json1.insert("A",90);

    json.insert("SetPositionRobot",json1);

    QJsonDocument doc(json);
    QString strJson(doc.toJson(QJsonDocument::Compact));

    client->write(strJson.toStdString().data());
    qDebug()<<"sended";
}
void Server::clientRead(){

    QByteArray rawData = client->readAll();

    // Parse document
    QJsonDocument doc(QJsonDocument::fromJson(rawData));

    // Get JSON object
    QJsonObject json = doc.object();

    qDebug()<<json.keys();
}
