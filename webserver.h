#ifndef WEBSERVER_H
#define WEBSERVER_H

#include <QString>
#include <QWebSocket>
#include <QWebSocketServer>
#include <QJsonObject>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>

class WebServer: public QObject
{
    Q_OBJECT
public:
    explicit WebServer(QObject *parent = 0);
    void send_packet(QString);
signals:
    void messageRecievation();
private slots:
    void newConnection();
    void readyRead(QString);
    QList<QWebSocket*> getClient();
private:
//    QWebSocket *mSocket;
    QWebSocketServer *webServer;
    QList<QWebSocket*> _clientList;
    QObject* _mainRefrence;

};

#endif // WEBSERVER_H
