#ifndef MAINCLASS_H
#define MAINCLASS_H

#include <QObject>
#include <unistd.h>
#include <QDebug>
#include <QJsonObject>
#include <QJsonDocument>
#include <QTimer>
#include <QSettings>
#include "webserver.h"
#include "configmanager.h"

class MainClass : public QObject
{
    Q_OBJECT
public:
    explicit MainClass(QObject *parent = 0);

signals:

public slots:
    void try_connect();
    void getTokenFromRobot();
    void GetRobotConfig(QJsonObject);
    void SetConfigToken(QJsonArray);
    void DoneConfigToken(QJsonObject);
    void ResetTokenList(QJsonObject);

private:

    WebServer* client;
    QString _host;
    int _port;

    bool _sendRobotPose;
    int _sendTimeInterval;
    QString linel;
    QStringList list;

    ConfigManager configManager;
};

#endif // MAINCLASS_H
