#include "mainclass.h"
#include "definitions.h"
#include <QJsonArray>

MainClass::MainClass(QObject *parent) : QObject(parent)
{
    client = new WebServer(this);
    configManager.load_config();
}

void MainClass::try_connect(){
    qDebug()<<"[MAIN] "<<"connect";
    usleep(50E5);
    delete client;
    client = new WebServer(this);
}

void MainClass::GetRobotConfig(QJsonObject obj){
    configManager.load_config();
    qDebug()<<"[MAIN] "<<"GetRobotConfig";
    QList<Token> tokenList = configManager.get_tokenList();
    QList<QFile*> fileList = configManager.get_qfileList();
    QString strJsonRobot;
    Q_FOREACH(QFile* tmp1 , fileList){
        QJsonObject fileName;
        QJsonObject fileAddress;
        QJsonArray data;
        fileName.insert("fileName",tmp1->fileName());
        data.append(fileName);
        Q_FOREACH(Token tmp , tokenList){
            if(tmp.fileAddress == tmp1->property("index").toInt()){
                QJsonObject robotData;
                robotData.insert("fileAddress",tmp.fileAddress);
                robotData.insert("value",tmp.value);
                robotData.insert("key",tmp.key);
                robotData.insert("tag",tmp.tag);
                robotData.insert("lineNumber",tmp.lineNumber);
                data.append(robotData);
                QJsonDocument docRobot (data);
                strJsonRobot = docRobot.toJson(QJsonDocument::Compact);
            }
        }
        client->send_packet(strJsonRobot);
        qDebug()<<strJsonRobot;
    }
}



void MainClass::getTokenFromRobot(){
    QJsonObject getToken;
    QJsonObject robotData;
    getToken.insert("GetRobotConfig",robotData);
    QJsonDocument docRobot(getToken);
    QString strJsonRobot(docRobot.toJson(QJsonDocument::Compact));
    client->send_packet(strJsonRobot);
}

void MainClass::SetConfigToken(QJsonArray obj)
{
    qDebug()<<"[MAIN] "<<"SetConfigToken";
    qDebug()<<"[MAIN] "<<obj;
    Token t;
    t.fileName = obj[0].toObject().value("fileName").toString();
    for(int i = 1 ; i< obj.size();i++){
        t.value = obj[i].toObject().value("value").toString();
        t.key = obj[i].toObject().value("key").toString();
        t.lineNumber = obj[i].toObject().value("lineNumber").toInt();
        t.tag = (Tags)obj[i].toObject().value("tag").toInt();
        t.fileAddress = obj[i].toObject().value("fileAddress").toInt();
        configManager.insert_token(t);
    }
}

void MainClass::DoneConfigToken(QJsonObject obj)
{
    qDebug()<<"[MAIN] "<<"DoneConfigToken";
    qDebug()<<"[MAIN] "<<obj;
    configManager.save_config();
}

void MainClass::ResetTokenList(QJsonObject obj)
{
    qDebug()<<"[MAIN] "<<"ResetTokenList";
    qDebug()<<"[MAIN] "<<obj;
    configManager.reset_tokenList();
}
