#include "configmanager.h"
#include <QFile>
#include <QTextStream>
#include <QJsonObject>
#include <QJsonDocument>

ConfigManager::ConfigManager()
{


}

void ConfigManager::load_config(){
    indexer = 0;
    for(int k=0;k<qlistFile.length();k++){
        if(!qlistFile[k]->open(QIODevice::ReadOnly)){
            qDebug()<<"Error In Reload The File!";
        }
    }
    qlistFile.clear();
    QString address=  "/Users/alirezamohafezatkar/Desktop/mrl/Player/Config/Config_OP.lua";
    QFile* otherFile = new QFile(address);
    qlistFile << otherFile;
    address.clear();
//    address = "/Users/alirezamohafezatkar/Desktop/mrl/Player/Config/World/Config_OP_World.lua";
//    otherFile = new QFile(address);
//    qlistFile << otherFile;
//    address.clear();
//    address=  "/Users/alirezamohafezatkar/Desktop/mrl/Player/Config/Vision/Config_OP_Vision.lua";
//    otherFile = new QFile(address);
//    qlistFile << otherFile;
    address.clear();
    for(int counter = 0;counter<qlistFile.length();counter++){
        qlistFile[counter]->setProperty("index",indexer);
        qlistFile[counter]->setProperty("fileName",fileNameList);
        indexer++;
    }
    tokenList.clear();

    int j = 0;

    QRegExp rx("\\$");
    QRegExp regexIP("(IP)");
    QRegExp regexNumber("(N)");
    QRegExp regexBoolean("(B)");
    QRegExp regexArray("(A)");
    QRegExp regexString("(S)");
    QRegExp rex("=|;|\\$");

    Token t;

    Q_FOREACH (QFile *tmp, qlistFile)
    {
            if(!tmp->open(QIODevice::ReadOnly))
            {
                qDebug()<<"Error In Opening The File!";
            }
            QTextStream ini(tmp);
            while(!ini.atEnd()){
                linel = ini.readAll();
                list = linel.split("\n");
                j=0;
                while(j<=list.length()-1){
                    if(rx.indexIn(list[j])!= -1)
                    {
                        finalFileList = list[j].split(rex);
                        t.key = finalFileList[0];
                        t.value = finalFileList[1];
                        if(regexIP.exactMatch(finalFileList[3])){
                            t.tag = Ip;
                            t.lineNumber = j;
                            QStringList cot1;
                            cot1 = t.value.split("'");
                            t.value = cot1[1];
                            t.fileAddress = tmp->property("index").toInt();
                            t.fileName = tmp->property("fileName").toString();
                        }
                        else if(regexNumber.exactMatch(finalFileList[3])){
                            t.tag = Number;
                            t.lineNumber = j;
                            t.fileAddress = tmp->property("index").toInt();
                            t.fileName = tmp->property("fileName").toString();
                        }
                        else if(regexBoolean.exactMatch(finalFileList[3])){
                            t.tag = Boolean;
                            t.lineNumber = j;
                            t.fileAddress = tmp->property("index").toInt();
                            t.fileName = tmp->property("fileName").toString();
                        }
                        else if(regexArray.exactMatch(finalFileList[3])){
                            t.tag = Array;
                            t.value = finalFileList[2];
                            t.value.append(":");
                            finalFileList[1].replace(" ","");
                            t.value.append(finalFileList[1]);
                            t.lineNumber = j;
                            t.fileAddress = tmp->property("index").toInt();
                            t.fileName = tmp->property("fileName").toString();
                        }
                        else if(regexString.exactMatch(finalFileList[3])){
                            t.tag = String;
                            t.lineNumber = j;
                            QStringList cot2;
                            cot2 = t.value.split("'");
                            t.value = cot2[1];
                            t.fileAddress = tmp->property("index").toInt();
                            t.fileName = tmp->property("fileName").toString();
                        }
                            tokenList.append(t);
                            j++;
                    }
                    else
                    {
                        j++;
                    }
                }
            }
            tmp->close();
    }
}



void ConfigManager::reset_tokenList()
{
    tokenList.clear();
}

void ConfigManager::insert_token(Token t)
{
    tokenList.append(t);
}

void ConfigManager::save_config(){
    QString s1;
    QString s2;
    QString s4;
    QString s3;
    QString s5;

    for(int k=0;k<qlistFile.length();k++){
        if(!qlistFile[k]->open(QIODevice::ReadWrite)){
            qDebug()<<"Error In Saving The File!";
        }
        QTextStream ini(qlistFile[k]);
        linel = ini.readAll();
        list = linel.split("\n");
        foreach (Token t, tokenList){
            if(t.fileAddress == qlistFile[k]->property("index").toInt()){
                if(t.tag == Ip){
                    s1 =t.key;
                    s1.append("= ");
                    s1.append("'");
                    s1.append(t.value);
                    s1.append("'; --$IP$");
                    list.replace(t.lineNumber, s1);
                    s1.clear();
                }

                if(t.tag == Number){
                    s2 =t.key;
                    s2.append("=");
                    s2.append(t.value);
                    s2.append("; --$N$");
                    list.replace(t.lineNumber, s2);
                    s2.clear();
                }

                if(t.tag == Boolean)
                {
                    s4 =t.key;
                    s4.append("=");
                    s4.append(t.value);
                    s4.append("; --$B$");
                    list.replace(t.lineNumber, s4);
                    s4.clear();
                }

                if(t.tag == String)
                {
                    s3 =t.key;
                    s3.append("= ");
                    s3.append("'");
                    s3.append(t.value);
                    s3.append("'; --$S$");
                    list.replace(t.lineNumber, s3);
                    s3.clear();
                }

                if(t.tag == Array)
                {
                    s5=t.key;
                    s5.append("= ");
                    QString value = t.value;
                    QStringList option = value.split(":");
                    s5.append(option[1]);
                    s5.append(";");
                    s5.append(option[0]);
                    s5.append("$A$");
                    list.replace(t.lineNumber, s5);
                    s5.clear();
                }
            }
        }
        QTextStream s(qlistFile[k]);
        s.seek(0);
        for (int i=0;i<=list.size()-1;i++){
            //I wrote this code because every time it was saved it was gereated one new line but now it's not
            if(i == list.size()-1){
                s<<list.at(i);
            }
            else{
                s<<list.at(i)<<"\n";
            }
        }
        qlistFile[k]->close();
    }
    indexer = 0;
    load_config();
}
