QT += core network websockets
QT -= gui

TARGET = RobotServer
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    mainclass.cpp \
    configmanager.cpp \
    webserver.cpp

HEADERS += \
    mainclass.h \
    configmanager.h \
    definitions.h \
    webserver.h
