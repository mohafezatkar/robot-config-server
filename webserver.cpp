#include "webserver.h"

WebServer::WebServer(QObject *parent)
{
    _mainRefrence = parent;
    webServer = new QWebSocketServer("WebServer",QWebSocketServer::NonSecureMode,parent);
    webServer->listen(QHostAddress::Any,1234);
    // whenever a user connects, it will emit signal
    connect(webServer, SIGNAL(newConnection()),this, SLOT(newConnection()));
}

void WebServer::newConnection()
{
    QWebSocket *socket = webServer->nextPendingConnection();
    connect(socket,&QWebSocket::textMessageReceived, this, &WebServer::readyRead);
        _clientList.append(socket);
}

void WebServer::readyRead(QString message){

    QByteArray rawData = message.toLocal8Bit();
    QJsonDocument doc(QJsonDocument::fromJson(rawData));
    QJsonObject json = doc.object();
    if(json.value("SetConfigToken").isArray()){
        QMetaObject::invokeMethod(_mainRefrence
                                  ,json.keys()[0].toStdString().data()
                ,Q_ARG(QJsonArray,json[json.keys()[0]].toArray()));
    }
    else if(!json.isEmpty()){
        QMetaObject::invokeMethod(_mainRefrence
                                  ,json.keys()[0].toStdString().data()
                ,Q_ARG(QJsonObject,json[json.keys()[0]].toObject()));
    }

}


QList<QWebSocket *> WebServer::getClient()
{
    return _clientList;
}

void WebServer::send_packet(QString packet){
    Q_FOREACH(QWebSocket* socket,_clientList){
        socket->sendTextMessage(packet);
    }
}
